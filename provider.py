from inputs import value_input
from inputs import boolean_input
from inputs import category_input
from inputs import normalize_input

from outputs import category_output

# from gpu_service import GpuService

valueInput = value_input.ValueInput()
booleanInput = boolean_input.BooleanInput()
categoryInput = category_input.CategoryInput()
normalizeInput = normalize_input.NormalizeInput()
categoryOutput = category_output.CateogryOutput()
# gpuService = GpuService()