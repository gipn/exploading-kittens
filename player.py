import player_state
import listeners
import random
import datetime
import log
import config
import debug

class Player(listeners.DeckListener):
    def __init__(self, model):
        self.__cards = []
        self.__state = player_state.PlayerState()
        self.__game = None
        self.id = 0
        self.__turns = 0
        self.__score = 0
        self.__model = model
        self.onShuffle()

    def __del__(self):
        self.cleanUp()

    def cleanUp(self):
        if (self.__game is not None):
            self.__game.getCardDeck().removeListener(self)

    def setId(self, id):
        self.id = id

    def initGame(self, id, game):
        self.setId(id)
        self.cleanUp()
        self.__game = game
        self.__game.getCardDeck().addListener(self)

    def addCard(self, card):
        log.debug("Player " + str(self.id) + " added card " + str(card.id))
        self.__cards.append(card)

    def removeCard(self, card):
        self.__cards.remove(card)

    def getCards(self):
        return self.__cards

    def hasCard(self, cardName):
        return self.countCard(cardName) > 0

    def countCard(self, cardName):
        count = 0
        for card in self.__cards:
            if (card.id != cardName):
                continue
            count += 1
        return count

    def expload(self):
        self.makeDecision(self.__game)
        if (self.hasCard("bomb")):
            log.debug("Player: " + str(self.id) + " exploaded")
            self.__state.setState("dead")

    def endTurn(self):
        self.__state.setState("end")

    def skip(self):
        self.__state.setState("skip")

    def playTurn(self, game):
        self.__turns += 1
        self.__state.reset()

        self.__state.setState("turn")
        while (self.__state.isState("turn")):
            self.makeDecision(game)
        if (self.__state.isState("skip")):
            return
        game.getCardDeck().takeCard(self)

    def giveCard(self):
        if (len(self.__cards) == 0):
            return None
        cardIndex = random.randint(0, len(self.__cards) - 1)
        card = self.__cards[cardIndex]
        self.getCards().remove(card)
        return card

    def takeCardFrom(self, oponent):
        card = oponent.giveCard()
        if (card is None):
            return
        self.addCard(card)

    def makeDecision(self, game):
        log.debug("Player " + str(self.id) + " making decision " + self.__cardsToString() + self.__cardKnowledgeToString())
        action = self.__model.getAction(game, self)
        action.execute(game, self)

    def setKnowledge(self, index, card):
        self.__cardKnowledge[index] = card

    def getFuture(self, count):
        deck = self.__game.getCardDeck()
        cardIndex = deck.getCurrentCardIndex()
        future = []
        i = 0
        while (i < count):
            future.append(self.__cardKnowledge[cardIndex + i])
            i += 1
        return future

    def isDead(self):
        return self.__state.isState("dead")

    def onShuffle(self):
        self.__cardKnowledge = []
        for i in range(100):
            self.__cardKnowledge.append(False)
        return

    def onTakeCard(self, player):
        return

    def getScore(self):
        score = self.__turns
        if (not self.isDead()):
            score += self.__game.getCardDeck().countRemaining() + config.PTS_WIN
        return score

    def __getCardsNames(self, cards):
        cardNames = []
        for card in cards:
            name = "None"
            if (card):
                name = card.id
            cardNames.append(name)
        return str(cardNames)

    def __cardsToString(self):
        return str(self.__getCardsNames(self.__cards))

    def __cardKnowledgeToString(self):
        return str(self.__getCardsNames(self.getFuture(3)))
