import config

LOG_LEVEL_DEBUG = 0
LOG_LEVEL_INFO = 1
LOG_LEVEL_WARNING = 2
LOG_LEVEL_ERROR = 3

def log(text):
    print(text)

def logLevel(text, level):
    if (config.LOG_LEVEL > level):
        return ""
    print(text)

def info(text):
    logLevel(text, LOG_LEVEL_INFO)

def debug(text):
    if (not config.DEBUG):
        return
    print(text)

