class CateogryOutput:
    def getMaxIndex(self, probabilities):
        i = 0
        iMax = -1
        for p in probabilities:
            if (iMax == -1 or p > probabilities[iMax]):
                iMax = i
            i += 1
        return iMax

    def transform(self, probabilities, options = {}):
        maxIndex = self.getMaxIndex(probabilities)
        if ("map" in options):
            return options["map"][maxIndex]
        return maxIndex