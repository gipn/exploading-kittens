class CategoryInput:

    def transform(self, value, options = {}):
        arr = []
        start = 0
        if ("start" in options):
            start = options["start"]
        for i in range(options["limit"]):
            if (value == i + start):
                arr.append(1.0)
            else:
                arr.append(0.0)
        return arr
    
    def transformSize(self, options = {}):
        return options["limit"]