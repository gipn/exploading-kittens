class BooleanInput:
    def transform(self, value, options = {}):
        boolean = 1.0
        if (value < 0.5):
            boolean = -1.0
        return [boolean]

    def transformSize(self, options = {}):
        return 1
        