class NormalizeInput:

    def transform(self, value, options = {}):
        minValue = 0
        if ("min" in options):
            minValue = options["min"]
        norm = (value - minValue) / (options["max"] - minValue)
        return  [norm]

    def transformSize(self, options = {}):
        return 1
