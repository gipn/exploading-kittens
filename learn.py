from evolution import Evolution
from individual import Individual
from model import Model
import exploading_kittens as ek
import log
import config

evolution = Evolution(config.EVO_GENERATION_LIMIT)
populations = evolution.run()
i = 0
for p in populations:
    i += 1
    log.log("POPULATION: " + str(i))
    for t in p.getTopIndividuals(3):
        log.log(t.getWeights())