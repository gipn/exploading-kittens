from network import Network
from iterable import Iterable
import config

class Individual:
    def __init__(self, weights):
        self.__weights = weights
        self.resetScore()
        self.__net = Network(self.iterator())
        layers = config.NETWORK_LAYERS
        self.__net.inputLayer(layers[0])
        for i in layers[1:-1]:
            self.__net.hiddenLayer(i)
        self.__net.outputLayer(layers[len(layers) - 1])

    def getWeights(self):
        return self.__weights

    def iterator(self):
        return Iterable(self.getWeights())

    def blockIterator(self):
        blocks = [];
        blockSizes = config.getWeightBlocks()
        start = 0
        end = 0
        for size in blockSizes:
            end += size
            blocks.append(self.__weights[start:end])
            start = end
        return Iterable(blocks)

    def addScore(self, score):
        self.__score += score
        self.__countGames += 1

    def resetScore(self):
        self.__score = 0.0
        self.__countGames = 0

    def getAvgScore(self):
        if (self.__countGames == 0):
            return 0.0
        return self.__score / self.__countGames

    def getNetwork(self):
        return self.__net
