class Factory:
    def __init__(self):
        self.__pairs = {}

    def add(self, name, value):
        self.__pairs[name] = value

    def create(self, cardName):
        if (cardName not in self.__pairs):
            return None
        constructor = self.__pairs[cardName]
        return constructor()