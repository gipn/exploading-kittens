from iterable import Iterable
from individual import Individual
from model import Model
from evo import population
import random
import config
import exploading_kittens as ek
import log
import datetime
from settings.population import POPULATIONS

class Evolution:
    def __init__(self, generationsLimit):
        self.__generationsLimit = generationsLimit
        self.__playersLimit = 3
        self.__populations = []
        for i in range(self.__playersLimit):
            weights = []
            if (config.CONTINUE and len(POPULATIONS) > i):
                weights = POPULATIONS[i]
            self.__populations.append(population.Population(config.EVO_INDIVIDUALS_LIMIT, config.EVO_TOP_LIMIT, weights))
        self.__currentPopulation = 0

    def writeProgress(self, generationNumber):
        outputFile = open("storage/output/" + self.__run_start.strftime("%Y-%m-%d-%H-%M-%S") + ".txt", "a")
        outputFile.write("[" + self.__run_start.isoformat() + "]: Generation " + str(generationNumber) + "\n")
        i = 1
        for p in self.__populations:
            outputFile.write("Population: " + str(i) + "\n")
            j = 1
            for individual in p.getIndividuals():
                outputFile.write(str(j) + ". (" +str(individual.getAvgScore()) + ") " + str(individual.getWeights()) + "\n")
                j += 1
            i += 1
        outputFile.close()

    def run(self):
        self.__run_start = datetime.datetime.now()
        for i in range(self.__generationsLimit):
            if (i % config.WRITE_EVERY_GENERATION == 0):
                self.writeProgress(i)
            log.log("GENERATION: " + str(i))
            for p in self.__populations:
                p.resetStats()
                p.breed()
                self.cycle(p)
                p.selection()                
                print(p.getStats())
        return self.__populations

    def createPlayers(self, individuals):
        players = []
        for ind in individuals:
            players.append(ek.createPlayer(Model(ind.getNetwork())))
        return players

    def evaluate(self, individual, populations):
        i = 0
        individual.resetScore()
        while (True):
            pInd = []
            pInd.append(individual)
            for p in populations:
                ind = p.getIndividual(i)
                if (ind is None):
                    return individual.getAvgScore()
                pInd.append(ind)
            players = self.createPlayers(pInd)
            game = self.getGame(players)
            game.play()
            for j in range(self.__playersLimit):
                pInd[j].addScore(players[j].getScore())
            i += 1

    def cycle(self, population):
        oponentPopulations = []
        for p in self.__populations:
            if (p == population):
                continue
            oponentPopulations.append(p)
        for individual in population.getIndividuals():
            self.evaluate(individual, oponentPopulations)
                
    def getGame(self, players):
        game = ek.Game()
        deck = ek.createDeck()
        game.setCardDeck(deck)

        for player in players:
            game.addPlayer(player)
            player.addCard(deck.getCardFactory().create("defuse"))

        for (cardName, count) in config.DECK_CARDS.items():
            for _ in range(count):
                deck.createCard(cardName)
        for _ in range(len(players) - 1):
            deck.createCard("bomb")

        deck.shuffle()
        return game