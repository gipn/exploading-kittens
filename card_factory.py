from cards import *

class CardFactory:
    def __init__(self):
        self.__pairs = {
            "tacocat": card_tacocat.TacocatCard,
            "bomb": card_bomb.BombCard,
            "skip": card_skip.SkipCard,
            "see_the_future": card_see_the_future.SeeTheFutureCard,
            "defuse": card_defuse.DefuseCard,
            "shuffle": card_shuffle.ShuffleCard,
            "beardcat": card_beardcat.BeardcatCard,
            "potatocat": card_potatocat.PotatocatCard
        }

    def create(self, cardName):
        if (cardName not in self.__pairs):
            return None
        constructor = self.__pairs[cardName]
        return constructor()