from cards.card import Card
import config

class BombCard(Card):
    def __init__(self):
        super(BombCard, self).__init__("bomb", 1)

    def pickUp(self, player):
        super(BombCard, self).pickUp(player)
        self.execute(None, player)

    def execute(self, game, player):
        player.expload()