from cards.card import Card
import random
import log
import config

class DefuseCard(Card):
    def __init__(self):
        super(DefuseCard, self).__init__("defuse", 2)

    def execute(self, game, player):
        if (not player.hasCard("bomb")):
            return
        cardDeck = game.getCardDeck()
        cards = player.getCards()
        for card in cards:
            if (card.id != "bomb"):
                continue
            log.info("BOMB DEFUSED")
            cards.remove(card)
            cardDeck.insertCard(card, random.randint(0, cardDeck.countRemaining()));
            return