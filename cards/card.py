import config

class Card:
    def __init__(self, id = 0, numberId = 0):
        self.id = id
        self.numberId = numberId

    def pickUp(self, player):
        player.addCard(self)

    def execute(self, game, player):
        return
