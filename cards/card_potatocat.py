from cards.card import Card

class PotatocatCard(Card):
    def __init__(self):
        super(PotatocatCard, self).__init__("potatocat", 8)