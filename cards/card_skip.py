from cards.card import Card
import log
import config

class SkipCard(Card):
    def __init__(self):
        super(SkipCard, self).__init__("skip", 5)

    def execute(self, game, player):
        log.info("Player: " + str(player.id) + " skipped")
        player.skip()
        future = player.getFuture(1)
        futureCard = future[0]