from cards.card import Card

class TacocatCard(Card):
    def __init__(self):
        super(TacocatCard, self).__init__("tacocat", 3)