from cards.card import Card

class BeardcatCard(Card):
    def __init__(self):
        super(BeardcatCard, self).__init__("beardcat", 7)