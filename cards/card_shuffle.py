from cards.card import Card
import log
import config

class ShuffleCard(Card):
    def __init__(self):
        super(ShuffleCard, self).__init__("shuffle", 6)

    def execute(self, game, player):
        log.info("shuffle")
        deck = game.getCardDeck();
        deck.shuffle()