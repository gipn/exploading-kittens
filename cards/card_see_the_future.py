from cards.card import Card
import log
import config

class SeeTheFutureCard(Card):
    def __init__(self):
        super(SeeTheFutureCard, self).__init__("see_the_future", 4)

    def execute(self, game, player):
        log.info("RUN SEE THE FUTURE")
        deck = game.getCardDeck();
        cards = deck.peek(3);
        future = player.getFuture(3)
        i = 0
        for card in cards:
            player.setKnowledge(deck.getCurrentCardIndex() + i, card)
            i += 1