import pyopencl as cl
import numpy

class GpuService:
    def __init__(self):
        self.__ctx = cl.create_some_context()
        self.__queue = cl.CommandQueue(self.__ctx)
        self.__program = cl.Program(self.__ctx, """
            __kernel void product(
                __global const float *a, __global const float *b, __global float *result)
            {
            int gid = get_global_id(0);
            result[0] += a[gid] * b[gid];
            }
            """).build()

    def createReadBuffer(self, data):
        return cl.Buffer(self.__ctx, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=data)

    def createWriteBuffer(self, numberOfBytes):
        return cl.Buffer(self.__ctx, cl.mem_flags.WRITE_ONLY, numberOfBytes)

    def product(self, weights, values):
        weightsBuffer = self.createReadBuffer(weights)
        valuesBuffer = self.createReadBuffer(values)
        resultBuffer = self.createReadBuffer(numpy.array([0.0], numpy.float))
        self.__program.product(self.__queue, weights.shape, None, weightsBuffer, valuesBuffer, resultBuffer)
        result = numpy.array([0.0], numpy.float)
        cl.enqueue_copy(self.__queue, result, resultBuffer)
        return result[0]
