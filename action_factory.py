from actions import *

class ActionFactory:
    def __init__(self):
        self.__pairs = {
            "empty": empty_action.EmptyAction,
            "card": card_action.CardAction,
            "two_of_a_kind": two_of_a_kind_action.TwoOfAKindAction
        }

    def create(self, name, args = []):
        if (name not in self.__pairs):
            return None
        constructor = self.__pairs[name]
        return constructor(*args)