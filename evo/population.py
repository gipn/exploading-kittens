from individual import Individual
import random
import config
import log

class Population:

    def __init__(self, individualsLimit, topLimit, weights):
        self.__individualsLimit = individualsLimit
        self.__topLimit = topLimit
        self.__individuals = self.initIndividuals(weights)
        self.resetStats()

    def resetStats(self):
        self.__stats = {
            "mutations": 0,
            "mutate_gene": 0,
            "mutate_flip": 0,
            "create_individuals": 0
        }

    def getStats(self):
        return self.__stats

    def getIndividuals(self):
        return self.__individuals

    def getIndividual(self, index):
        if (index >= len(self.__individuals)):
            return None
        return self.__individuals[index]

    def initIndividuals(self, weights = []):
        individuals = []
        i = 0
        while (i < self.__topLimit):
            if (i >= len(weights)):
                weights.append([config.generateWeight() for j in range(config.getWeightSize())])
            individuals.append(Individual(weights[i]))
            i += 1
        return individuals

    def crossover(self, mother, father):
        weights = []
        motherIter = mother.blockIterator()
        fatherIter = father.blockIterator()
        while (motherIter.hasNext()):
            m = motherIter.next()
            f = fatherIter.next()
            r = random.random()
            if (r <= config.MOTHER_GENE_PROBABILITY_TRESHHOLD):
                w = m
            else:
                w = f
            weights.extend(w)
        return Individual(weights)

    def mutation(self, individual):
        self.__stats["mutations"] += 1
        weights = []
        i = individual.iterator()
        mutationTreshhold = config.GENE_MUTATION_PROBABILITY_TRESHHOLD * random.random()
        while (i.hasNext()):
            w = i.next()
            r = random.random()
            if (r < mutationTreshhold):
                self.__stats["mutate_gene"] += 1
                w *= (random.random() + 0.5)
                if (random.random() < 0.5):
                    self.__stats["mutate_flip"] += 1
                    w *= -1
            weights.append(w)
        return Individual(weights)

    def breed(self):
        size = len(self.__individuals)
        children = []
        if (config.EVO_ELITIST):
            children.extend(self.__individuals)
        while (len(children) < self.__individualsLimit):
            self.__stats["create_individuals"] += 1
            i = random.randint(0, size - 1)
            while True:
                j = random.randint(0, size - 1)
                if (j != i):
                    break
            child = self.crossover(self.__individuals[i], self.__individuals[j])
            if (random.random() < config.MUTATION_PROBABILITY_TRESHHOLD):
                child = self.mutation(child)
            children.append(child)
        self.__individuals = children

    def getTopIndividuals(self, limit = False):
        if (limit == False):
            limit = self.__topLimit
        self.__individuals.sort(key=sortIndividuals, reverse=True)
        return self.__individuals[:limit]

    def selection(self):
        self.__individuals = self.getTopIndividuals()
        limit = len(self.__individuals)
        log.log("TOP:")
        for i in range(limit):
            log.log(str(i) + " = " + str(self.__individuals[i].getAvgScore()))
        return self.getIndividuals()

def sortIndividuals(individual):
    return individual.getAvgScore()