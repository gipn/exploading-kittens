import config
from nn import input_neuron
from nn import sum_sigmoid_neuron
from nn import bias_neuron

class Network:
    def __init__(self, weights):
        self.__weights = weights

    def setLastLayer(self, lastLayer):
        self.__lastLayer = lastLayer

    def addBias(self):
        if (not config.HAS_BIAS):
            return
        self.__lastLayer.append(bias_neuron.BiasNeuron())

    def inputLayer(self, size):
        self.__inputNodes = []
        for _ in range(size):
            self.__inputNodes.append(input_neuron.InputNeuron())
        self.setLastLayer(self.__inputNodes)
        self.addBias()

    def layer(self, size):
        layer = []
        lastLayer = self.__lastLayer
        for _ in range(size):
            node = sum_sigmoid_neuron.SumSigmoidNeuron()
            for iNode in lastLayer:
                node.addInput(iNode, self.__weights.next())
            layer.append(node)
        self.setLastLayer(layer)

    def hiddenLayer(self, size):
        self.layer(size)
        self.addBias()

    def outputLayer(self, size):
        self.layer(size)

    def createDataset(self, inputs):
        dataset = {}
        i = 0
        for inputValue in inputs:
            dataset[self.__inputNodes[i]] = inputValue
            i += 1
        return dataset

    def run(self, inputValues):
        outputValues = []
        inputSize = len(inputValues)
        for i in range(inputSize):
            self.__inputNodes[i].setValue(inputValues[i])
        for output in self.__lastLayer:
            outputValues.append(output.evaluate())
        return outputValues


# import tensorflow as tf
# 
# class Network:
#     def __init__(self, weights):
#         self.__weights = weights
#         self.__graph = tf.Graph()

#     def setLastLayer(self, lastLayer):
#         self.__lastLayer = lastLayer

#     def inputLayer(self, size):
#         self.__inputNodes = []
#         with self.__graph.as_default():
#             for i in range(size):
#                 self.__inputNodes.append(tf.placeholder(tf.float32))
#         self.setLastLayer(self.__inputNodes)

#     def hiddenLayer(self, size):
#         layer = []
#         lastLayer = self.__lastLayer
#         with self.__graph.as_default():
#             for i in range(size):
#                 node = tf.constant(0.0, tf.float32)
#                 for iNode in lastLayer:
#                     node += iNode * tf.constant(self.__weights.next(), tf.float32)
#                 layer.append(tf.sigmoid(node))
#         self.setLastLayer(layer)

#     def outputLayer(self, size):
#         self.hiddenLayer(size)

#     def createDataset(self, inputs):
#         dataset = {}
#         i = 0
#         for inputValue in inputs:
#             dataset[self.__inputNodes[i]] = inputValue
#             i += 1
#         return dataset

#     def run(self, inputValues):
#         outputValues = [];
#         sess = tf.Session(graph=self.__graph)
#         for output in self.__lastLayer:
#             outputValues.append(sess.run(output, self.createDataset(inputValues)))
#         sess.close()
#         return outputValues
