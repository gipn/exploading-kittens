from evolution import Evolution
from individual import Individual
from model import Model
import exploading_kittens as ek
import log
import config
import os

os.environ["PYOPENCL_CTX"] = "0:0"

evolution = Evolution(config.EVO_GENERATION_LIMIT)

playersLimit = 3
players = []
learnedWeights = config.MODEL
for p in range(playersLimit):
    individual = Individual(learnedWeights)
    players.append(ek.createPlayer(Model(individual.getNetwork())))
game = evolution.getGame(players)
game.play()