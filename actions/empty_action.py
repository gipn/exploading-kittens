from actions.actions_i import Action
import log

class EmptyAction(Action):
    def execute(self, game, player):
        log.debug("Play no action")
        player.endTurn()
        return