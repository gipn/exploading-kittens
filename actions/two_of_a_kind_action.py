from actions.actions_i import Action
import log

class TwoOfAKindAction(Action):
    def initialize(self, config):
        self.__cardName = config["cardName"]
        self.__oponent = config["player"]

    def execute(self, game, player):
        if (player.countCard(self.__cardName) < 2):
            log.debug("Want to play two of a kind: " + self.__cardName)
            player.endTurn()
            return 
        log.debug("Play two of a kind: " + self.__cardName)
        cards = player.getCards()
        count = 0
        for card in cards:
            if (card.id == self.__cardName):
                count += 1
                player.removeCard(card)
                game.discardCard(card)
            if (count >= 2):
                break
        player.takeCardFrom(self.__oponent)