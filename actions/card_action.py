from actions.actions_i import Action
import log

class CardAction(Action):
    def __init__(self, cardName):
        self.__cardName = cardName

    def execute(self, game, player):
        if (not player.hasCard(self.__cardName)):
            log.debug("Want to play card: " + self.__cardName)
            player.endTurn()
            return 
        log.debug("Play card: " + self.__cardName)
        cards = player.getCards()
        for card in cards:
            if (card.id == self.__cardName):
                log.debug("Playing card: " + self.__cardName)
                game.playCard(card, player)
                return