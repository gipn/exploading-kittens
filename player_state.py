class PlayerState:
    def __init__(self):
        self.__state = ""
        self.reset()

    def reset(self):
        self.__state = ""

    def setState(self, name):
        self.__state = name

    def isState(self, name):
        return self.__state == name