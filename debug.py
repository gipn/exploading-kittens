import time
import log

class DebugTimer:
    def __init__(self, name):
        self.__start = time.time()
        self.__name = name

    def getElapsedTime(self):
        return self.__end - self.__start

    def end(self):
        self.__end = time.time()
        log.debug("TIMER '" + self.__name + "': " + str(self.getElapsedTime()))

    def print(self):
        log.log(self.getElapsedTime())

def timerStart(name):
    return DebugTimer(name)