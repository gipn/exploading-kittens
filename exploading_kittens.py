import card_deck
from player import Player
import debug

class Game:
    def __init__(self):
        self.__cardDeck = None
        self.__discardPile = card_deck.CardDeck()
        self.__players = []
        self.__winner = None
        self.__currentPlayer = 0
        self.__activePlayers = 0

    def setCardDeck(self, deck):
        self.__cardDeck = deck

    def getCardDeck(self):
        return self.__cardDeck

    def addPlayer(self, player):
        self.__players.append(player)
        self.__activePlayers += 1
        player.initGame(self.__activePlayers, self)
        return self

    def currentPlayer(self):
        while True:
            player = self.__players[self.__currentPlayer]
            self.__currentPlayer += 1
            if (self.__currentPlayer >= len(self.__players)):
                self.__currentPlayer = 0
            if (not player.isDead()):
                break
        return player

    def playerLost(self, player):
        self.__activePlayers -= 1
        if (self.__activePlayers == 1):
            for p in self.__players:
                if (not p.isDead()):
                    self.__winner = p

    def play(self):
        while (not self.isEnd()):
            self.step()

    def discardCard(self, card):
        self.__discardPile.addCard(card)

    def playCard(self, card, player):
        player.getCards().remove(card)
        self.discardCard(card)
        card.execute(self, player)
        return

    def getOponent(self, playerIndex, excludePlayer):
        oponents = self.getOponents(excludePlayer)
        return oponents[playerIndex]

    def getOponents(self, excludePlayer):
        oponents = []
        for p in self.__players:
            if (p == excludePlayer):
                continue
            oponents.append(p)
        return oponents

    def step(self):
        if (self.isEnd()):
            return
        currentPlayer = self.currentPlayer()
        currentPlayer.playTurn(self)
        if (currentPlayer.isDead()):
            self.playerLost(currentPlayer)

    def getPlayers(self):
        return self.__players

    def isEnd(self):
        return (self.__winner is not None or self.__cardDeck.countRemaining() == 0)

    def getWinner(self):
        return self.__winner


def createDeck():
    return card_deck.CardDeck()

def createPlayer(model):
    return Player(model)
