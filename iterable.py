class Iterable:
    def __init__(self, arr):
        self.__arr = arr
        self.reset()

    def reset(self):
        self.__index = 0

    def hasNext(self):
        return self.__index < len(self.__arr)

    def next(self):
        if (not self.hasNext()):
            return None
        index = self.__index;
        self.__index += 1
        return self.__arr[index]

    def index(self):
        return self.__index

    def size(self):
        return len(self.__arr)

    def getArray(self):
        return self.__arr
