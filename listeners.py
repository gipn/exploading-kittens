class DeckListener:
    def onShuffle(self):
        raise NotImplementedError

    def onTakeCard(self, player):
        raise NotImplementedError