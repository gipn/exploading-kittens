from nn import input_neuron

class BiasNeuron(input_neuron.InputNeuron):
    def __init__(self, value = 0.0):
        super(BiasNeuron, self).__init__(1.0)