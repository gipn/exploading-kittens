from nn import neuron

class WeightNeuron(neuron.Neuron):

    def __init__(self, neuron, weight):
        self.__weight = weight
        self.__neuron = neuron

    def evaluate(self):
        return self.__weight * self.__neuron.evaluate()