import numpy
import log
from nn import neuron
from nn import weight_neuron
import math
import provider

class SumSigmoidNeuron(neuron.Neuron):

    def __init__(self):
        self.__neurons = []
        # self.__weights = []
        # self.__inputs = []

    def addInput(self, inputNeuron, weight):
        # self.__weights.append(weight)
        # self.__inputs.append(inputNeuron)
        self.__neurons.append(weight_neuron.WeightNeuron(inputNeuron, weight))

    def evaluate(self):
        neuronSum = 0
        for neuron in self.__neurons:
            neuronSum += neuron.evaluate()
        return (1 / (1 + math.exp(-1 * neuronSum)))

        # npWeights = numpy.array(self.__weights, numpy.float)
        # inputValues = []
        # for inputNeuron in self.__inputs:
        #     inputValues.append(inputNeuron.evaluate())
        # npInputs = numpy.array(inputValues, numpy.float)
        # neuronSum = provider.gpuService.product(npWeights, npInputs)
        # return (1 / (1 + math.exp(-1 * neuronSum)))
