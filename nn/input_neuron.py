from nn import neuron

class InputNeuron(neuron.Neuron):
    def __init__(self, value = 0.0):
        self.setValue(value)

    def setValue(self, value):
        self.__value = value;

    def evaluate(self):
        return self.__value