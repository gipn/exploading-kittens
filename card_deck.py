import random
import card_factory

class CardDeck:
    def __init__(self):
        self.__cards = []
        self.__cardFactory = card_factory.CardFactory()
        self.__listeners = []
        self.__currentCardIndex = 0
        self.__maxSize = 0

    def addCard(self, card):
        self.__cards.append(card)
        if (self.__maxSize < self.countRemaining()):
            self.__maxSize = self.countRemaining()

    def insertCard(self, card, index):
        self.__cards.insert(index, card)
        self.onShuffle()

    def createCard(self, cardName):
        self.addCard(self.__cardFactory.create(cardName))

    def takeCard(self, player):
        card = self.__cards.pop()
        self.__currentCardIndex += 1
        card.pickUp(player)
        self.onTakeCard(player)
        return card

    def peek(self, length):
        result = []
        i = len(self.__cards) - 1
        while (i >= 0):
            result.append(self.__cards[i])
            i -= 1
            if (len(result) == 3):
                break
        return result

    def getCurrentCardIndex(self):
        return self.__currentCardIndex

    def shuffle(self):
        random.shuffle(self.__cards);

    def countRemaining(self):
        return len(self.__cards);

    def remaining(self):
        return self.countRemaining() / self.__maxSize

    def getCardFactory(self):
        return self.__cardFactory

    def onTakeCard(self, player):
        for l in self.__listeners:
            l.onTakeCard(player)

    def onShuffle(self):
        for l in self.__listeners:
            l.onShuffle()

    def addListener(self, listener):
        self.__listeners.append(listener)

    def removeListener(self, listener):
        self.__listeners.remove(listener)