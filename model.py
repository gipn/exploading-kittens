import config
import random
from collections import OrderedDict
from network import Network
import provider

class Model:

    def __init__(self, net):
        self.__net = net

    def getNetwork(self, inputSize, outputSize):
        return self.__net

    def outputToDictionary(self, output):
        steps = {
            "actionProbability": config.OUTPUT_ACTIONS,
            "cardProbability": config.countCardTypes(),
            "playerProbability": config.PLAYERS_COUNT - 1
        }
        d = {}
        start = 0
        end = 0
        for (key, value) in steps.items():
            end += value
            d[key] = output[start:end]
            start = end
        return d

    def getAction(self, game, player):
        inputs = self.getInputs(game, player)
        actions = config.getActions()
        net = self.getNetwork(len(inputs), len(actions))
        actionProbability = net.run(inputs)
        actionDictionary = self.outputToDictionary(actionProbability)
        iMax = provider.categoryOutput.transform(actionDictionary["actionProbability"])
        actionConfig = {
            "player": provider.categoryOutput.transform(actionDictionary["playerProbability"], {"map": game.getOponents(player)}),
            "cardName": provider.categoryOutput.transform(actionDictionary["cardProbability"], {"map": config.getCardTypes()})
        }
        keys = list(actions.keys())
        action = actions.get(keys[iMax])
        action.initialize(actionConfig)
        return action

    def getInputs(self, game, player):
        inputs = []
        inputs.extend(provider.valueInput.transform(game.getCardDeck().remaining()))
        future = player.getFuture(3)
        futureCount = 0
        for card in future:
            if (not card):
                cardId = 0
            else:
                cardId = card.numberId
                futureCount += 1
            inputs.extend(provider.categoryInput.transform(cardId, {"limit": config.countCardTypes(), "start": 1}))
        inputs.extend(provider.normalizeInput.transform(futureCount, {"max": 3}))
        histogram = self.getHistogram(player.getCards())
        for key, value in histogram.items():
            inputs.extend(provider.normalizeInput.transform(value, {"max": 5}))
        return inputs

    def getHistogram(self, cards):
        histogram = OrderedDict();
        cardTypes = config.getCardTypes()
        for cardType in cardTypes:
            histogram[cardType] = 0

        for card in cards:
            histogram[card.id] += 1
        return histogram

